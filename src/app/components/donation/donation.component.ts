import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-donation',
  templateUrl: './donation.component.html',
  styleUrls: ['./donation.component.css']
})
export class DonationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
   donation:string;
   email:string;
   
    handleChange(evt) {
      var target = evt.target;
      if (target.checked) {
        let element = document.getElementById("button") as HTMLButtonElement;
        element.disabled = false;
        this.donation = target.getAttribute('data-dona');        
      } 
    }

    submit(){ 
      var em = document.getElementById("email") as HTMLInputElement; 
      this.email = em.value;
      alert(this.donation+' by '+this.email);
    }
}
